/* eslint-disable @typescript-eslint/no-require-imports */
const path = require('path')
const { merge } = require('webpack-merge')
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

const mode = process.env.NODE_ENV || 'development'

const commonConfig = {
  output: {
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    plugins: [new TsconfigPathsPlugin()],
    extensions: ['.js', '.ts', '.tsx', '.pug'],
  },
  mode,
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
      },
    ],
  },
}

const beConfig = merge(commonConfig, {
  entry: '~/be/index.ts',
  output: {
    filename: 'backend.js',
    devtoolModuleFilenameTemplate: 'file://[absolute-resource-path]',
  },
  target: 'node',
  devtool: 'inline-source-map',
  node: {
    __dirname: false,
  },
  optimization: {
    minimize: false,
  },
})

const feConfig = merge(commonConfig, {
  entry: '~/fe/index.tsx',
  output: {
    filename: 'bundle.js',
  },
  target: 'web',
})

module.exports = [beConfig, feConfig]
