FROM node:18-alpine

ENV NODE_ENV production
ENV HOME /home/node

COPY --chown=node:node ./dist/ $HOME/app/

USER node
WORKDIR /home/node/app

EXPOSE 8443
CMD ["node", "./backend.js"]
