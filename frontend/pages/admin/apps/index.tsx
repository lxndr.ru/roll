import { ChangeEventHandler, FC, useState } from 'react'
import styled from '@emotion/styled'
import { AppList } from './components/AppList'
import { useApps } from './useApps'

export const AdminAppsPage: FC = () => {
  const [filter, setFilter] = useState('')
  const { isLoading, apps } = useApps({ filter })

  const handleFilterChange: ChangeEventHandler<HTMLInputElement> = ({ target }) =>
    setFilter(target.value)


  if (isLoading) {
    return <div>Loading...</div>
  }

  return (
    <Container>
      <FilterInput value={filter} onChange={handleFilterChange} />
      <AppList apps={apps} />
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: calc(100% - 40px);
`

const FilterInput = styled.input`
  display: block;
  width: 400px;
  margin: 0 auto 20px;
  border: 1px solid gray;
  border-radius: 2px;
  padding: 3px;

  &:focus {
    border: 1px solid black;
  }
`
