import { FC, useRef } from 'react'
import { useVirtualizer } from '@tanstack/react-virtual'
import styled from '@emotion/styled'
import { OwnedGame } from '~/be/steam'
import { AppRow } from './AppRow'

interface AppListProps {
  apps: OwnedGame[]
}

export const AppList: FC<AppListProps> = ({ apps }) => {
  const parentRef = useRef<HTMLDivElement>(null)

  const { getTotalSize, getVirtualItems, measureElement } = useVirtualizer({
    count: apps.length,
    getScrollElement: () => parentRef.current,
    estimateSize: () => 45,
  })

  return (
    <Container ref={parentRef}>
      <div
        style={{
          height: getTotalSize(),
          position: 'relative',
        }}
      >
        {getVirtualItems().map(({ key, index, start }) => {
          const app = apps[index]

          return (
            <AppRow
              key={key}
              data-index={index}
              ref={measureElement}
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                transform: `translateY(${start}px)`,
              }}
              app={app}
            />
          )
        })}
      </div>
    </Container>
  )
}

const Container = styled.div`
  flex-grow: 1;
  overflow: auto;
`
