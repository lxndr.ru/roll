import { CSSProperties, useMemo, forwardRef } from 'react'
import { useQueries } from 'react-query'
import styled from '@emotion/styled'
import { Row } from '~/fe/components/layout'
import { SteamAppLogo } from '~/fe/components/SteamAppLogo'
import { AppInfo } from '~/be/db/types'
import { AppInfoForm, initialAppInfo } from './AppInfoForm'
import { fetchAppDetails, fetchAppInfo, fetchAppSchemas, updateAppInfoRequest } from '../api'
import { useUser } from '~/fe/providers/user'
import { OwnedGame } from '~/be/steam'

interface AppRowProps {
  style: CSSProperties
  app: OwnedGame
}

export const AppRow = forwardRef<HTMLDivElement, AppRowProps>(({
  style,
  app: { appid },
}, ref) => {
  const { token } = useUser()

  const [
    appDetailsQuery,
    appSchemaQuery,
    appInfoQuery,
  ] = useQueries([
    {
      queryKey: ['admin:steam.app.details', appid],
      queryFn: () => fetchAppDetails(appid, token),
    },
    {
      queryKey: ['admin:steam.app.schema', appid],
      queryFn: () => fetchAppSchemas(appid, token),
    },
    {
      queryKey: ['admin:app.info', appid],
      queryFn: () => fetchAppInfo(appid, token),
    },
  ])

  const { data: details } = appDetailsQuery
  const { data: schema } = appSchemaQuery
  const { data: info } = appInfoQuery

  const updateAppInfo = async (appid: number, appInfo: AppInfo) => {
    await updateAppInfoRequest(appid, appInfo)
    appInfoQuery.refetch()
  }

  const handleSubmit = (values: AppInfo) => updateAppInfo(appid, values)

  const referredId = details?.data?.steam_appid
  const achievements = schema?.data?.availableGameStats?.achievements || []
  const achievementCount = achievements.length
  const statCount = schema?.data?.availableGameStats?.stats?.length || 0

  const initialValues = useMemo(
    () => ({ ...initialAppInfo, ...info?.data || initialAppInfo }),
    [info?.data],
  )

  return (
    <Container ref={ref} style={style}>
      <SteamAppLogo appid={appid} />

      <DetailsWrapper>
        <div>
          <span>ID: </span>
          <span>{appid}</span>
          {referredId !== appid && (
            <ReferredID>
              <span> (refers to </span>
              <span>{referredId}</span>
              <span>)</span>
            </ReferredID>
          )}
        </div>
        <div>{details?.data?.name}</div>
      </DetailsWrapper>

      <SchemaWrapper>
        <AchievementCount>
          {achievementCount
            ? `Achievements: ${achievementCount}`
            : 'No achievements'}
        </AchievementCount>
        <StatsCount>
          {statCount
            ? `Stats: ${statCount}`
            : 'No stats'}
        </StatsCount>
      </SchemaWrapper>

      <AppInfoForm
        availableAchievements={achievements}
        initialValues={initialValues}
        onSubmit={handleSubmit}
      />
    </Container>
  )
})

const Container = styled(Row)`
  &:not(:first-of-type) {
    border-top: 1px dashed gray;
    margin: 0;
    padding: 8px;
  }
`

const DetailsWrapper = styled.div`
  flex: 0 0 250px;
  margin-left: 10px;
  text-align: left;
`

const ReferredID = styled.span`
  color: red;
  font-style: italic;
`

const SchemaWrapper = styled.div`
  flex: 0 0 200px;
  margin-left: 10px;
  text-align: left;
`

const AchievementCount = styled.div`
  white-space: nowrap;
`

const StatsCount = styled.div`
  white-space: nowrap;
`
