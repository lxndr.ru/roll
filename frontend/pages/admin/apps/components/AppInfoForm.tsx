import styled from '@emotion/styled'
import { FC, memo } from 'react'
import Form, { Field } from 'rc-field-form'
import { equals } from 'ramda'
import { AppInfo } from '~/be/db/types'
import { Achievement } from '~/be/steam'
import { Col, Row } from '~/fe/components/layout'
import { Button } from '~/fe/components/Button'
import { Checkbox } from '~/fe/components/Checkbox'
import { AchievementList } from './AchievementList'

export const initialAppInfo: AppInfo = {
  comment: '',
  noFinishAchievement: false,
  finishAchievements: [],
  noGame: false,
}

interface AppInfoFormProps {
  availableAchievements: Achievement[]
  initialValues: AppInfo
  onSubmit: (values: AppInfo) => void
}

export const AppInfoForm: FC<AppInfoFormProps> = memo(({
  availableAchievements,
  initialValues,
  onSubmit,
}) => (
  <Form<AppInfo>
    noValidate
    initialValues={initialValues}
    onFinish={(values) => onSubmit({ ...initialAppInfo, ...values })}
  >
    {(values) => (
      <Container>
        <Row>
          <Col>
            <Row>
              <Field name="comment">
                <CommentInput rows={4} />
              </Field>
            </Row>

            <Row>
              <Field name="noGame" valuePropName="checked">
                <Checkbox label="not a game" />
              </Field>
            </Row>
          </Col>

          {!!availableAchievements.length && (
            <Col>
              <Row>
                <Field name="noFinishAchievement" valuePropName="checked">
                  <Checkbox label="no finish achievement" />
                </Field>
              </Row>

              <Row>
                <Field name="finishAchievements">
                  <AchievementList availableAchievements={availableAchievements} />
                </Field>
              </Row>
            </Col>
          )}
        </Row>

        {!equals(values, initialValues) && (
          <Row>
            <SaveButton type="submit">Save</SaveButton>
            <CancelButton type="reset">Cancel</CancelButton>
          </Row>
        )}
      </Container>
    )}
  </Form>
))

const Container = styled.div`
  flex: 1 0 auto;
  display: flex;
  flex-direction: column;
  text-align: left;

  > :not(:first-of-type) {
    margin-top: 5px;
  }
`

const CommentInput = styled.textarea`
  width: 100%;
`

const SaveButton = styled(Button)``

const CancelButton = styled(Button)`
  margin-left: 4px;
`
