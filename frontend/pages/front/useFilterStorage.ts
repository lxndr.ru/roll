import { useMemo } from 'react'
import { isBrowser } from '~/fe/util/isBrowser'
import { FilterFields } from './api'
import { defaultGameFilter } from './components/RandomGameForm'

const RANDOM_GAME_KEY = 'randomGame'

const fetchStoredFilter = () => {
  if (isBrowser() && window.localStorage) {
    const storageItem = window.localStorage.getItem(RANDOM_GAME_KEY)

    if (storageItem) {
      try {
        return JSON.parse(storageItem) as FilterFields
      } catch (err: any) {
        console.error(err.message)
      }
    }
  }

  return defaultGameFilter
}

export const useFilterStorage = () =>
  useMemo(() => ({
    storedFilter: fetchStoredFilter(),
    storeFilter(filter: FilterFields) {
      window.localStorage.setItem(RANDOM_GAME_KEY, JSON.stringify(filter))
    },
  }), [])
