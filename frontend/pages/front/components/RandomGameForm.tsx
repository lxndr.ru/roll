import { FC } from 'react'
import { useQuery } from 'react-query'
import Form, { Field, FormInstance } from 'rc-field-form'
import styled from '@emotion/styled'
import { Category, CompletionFilter, Genre, Platform } from '~/cm/types'
import { CATEGORIES_QUERY_KEY, GENRES_QUERY_KEY } from '~/fe/query-keys'
import { useUser } from '~/fe/providers/user'
import { Checkbox } from '~/fe/components/Checkbox'
import { fetchCategories, fetchGenres, FilterFields } from '../api'
import { Multiselect } from './Multiselect'
import { Section } from './Section'
import { RadioOption, RadioGroup } from './RadioGroup'

export const defaultGameFilter: FilterFields = {
  includeFree: true,
  achievementsOnly: false,
  platform: Platform.windows,
  completion: CompletionFilter.all,
  categories: [],
  genres: [],
}

interface RandomGameFormProps {
  form: FormInstance
  onSpinClick: (values: FilterFields) => void
}

export const RandomGameForm: FC<RandomGameFormProps> = ({
  form,
  onSpinClick,
}) => {
  const { token } = useUser()

  const { data: categories = [] } = useQuery<Category[]>({
    queryKey: CATEGORIES_QUERY_KEY,
    queryFn: () => fetchCategories(token),
    initialData: [],
  })

  const { data: genres = [] } = useQuery<Genre[]>({
    queryKey: GENRES_QUERY_KEY,
    queryFn: () => fetchGenres(token),
    initialData: [],
  })

  const completionOptions: RadioOption[] = [
    { value: CompletionFilter.all, label: 'All' },
    { value: CompletionFilter.neverPlayed, label: 'Never played' },
    { value: CompletionFilter.played, label: 'Played' },
    { value: CompletionFilter.unfinished, label: 'Unfinished' },
    { value: CompletionFilter.uncompleted, label: 'Uncompleted' },
  ]

  const platformOptions: RadioOption[] = [
    { value: Platform.windows, label: 'Windows' },
    { value: Platform.mac, label: 'Mac' },
    { value: Platform.linux, label: 'Linux' },
  ]

  return (
    <Form<FilterFields>
      form={form}
      initialValues={defaultGameFilter}
      onFinish={onSpinClick}
    >
      <Container>
        <Section>
          <Field name="includeFree" valuePropName="checked">
            <Checkbox label="Include free games" />
          </Field>
        </Section>
        <Section>
          <Field name="achievementsOnly" valuePropName="checked">
            <Checkbox label="With achievements only" />
          </Field>
        </Section>
        <Section label="Completion">
          <Field name="completion">
            <RadioGroup options={completionOptions} />
          </Field>
        </Section>
        <Section label="Platform">
          <Field name="platform">
            <RadioGroup options={platformOptions} />
          </Field>
        </Section>
        <Section label="Categories">
          <Field name="categories">
            <Multiselect items={categories} />
          </Field>
        </Section>
        <Section label="Genres">
          <Field name="genres">
            <Multiselect items={genres} />
          </Field>
        </Section>

        <ActionBar>
          <SpinButton type="submit">Spin</SpinButton>
        </ActionBar>
      </Container>
    </Form>
  )
}

const Container = styled.div`
  display: inline-block;
  max-width: 700px;
`

const ActionBar = styled.div`
  padding: 10px 0;
`

const SpinButton = styled.button`
  background: cornflowerblue;
  color: white;
  border: none;
  border-radius: 2px;
  font-size: 20px;
  font-weight: bold;
  padding: 10px 20px;
`
