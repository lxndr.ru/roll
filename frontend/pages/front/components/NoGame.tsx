import { FC } from 'react'
import styled from '@emotion/styled'

interface NoGameProps {
  onResetClick: () => void
}

export const NoGame: FC<NoGameProps> = ({ onResetClick }) => (
  <Container>
    No game found.
    <br />
    Try to change or <a href="#" onClick={onResetClick}>reset</a> the filters.
  </Container>
)

const Container = styled.div`
  height: 40px;
  padding: 5px;
`
