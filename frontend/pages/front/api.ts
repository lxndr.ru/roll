import { App, AppFilter, Category, Genre, UserAppStatus } from '~/cm/types'
import { apiRequest } from '~/fe/util/apiRequest'

export type FilterFields = Required<Omit<AppFilter, 'steamid' | 'skipIgnored'>>

export const fetchRandomGame = async (filter: FilterFields, token?: string) =>
  await apiRequest<App>({
    url: '/random',
    token,
    params: filter,
  })

export interface MarkGameOptions {
  appid: number
  status: string
}

export const markGame = async (appid: number, status: UserAppStatus, token?: string) => {
  await apiRequest<void>({
    url: `/games/${appid}/mark`,
    method: 'POST',
    data: { status },
    token,
  })
}

export const fetchCategories = (token?: string) =>
  apiRequest<Category[]>({ url: '/categories', token })

export const fetchGenres = (token?: string) =>
  apiRequest<Genre[]>({ url: '/genres', token })
