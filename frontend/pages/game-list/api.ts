import { sortBy, prop } from 'ramda'
import { App } from '~/cm/types'
import { apiRequest } from '~/fe/util/apiRequest'

const sortByName = sortBy<App>(prop('name'))

export const fetchGameList = async (token?: string) => {
  const list = await apiRequest<App[]>({
    url: '/games',
    token,
  })

  return sortByName(list)
}
