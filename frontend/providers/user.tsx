import { FC, createContext, useContext, useMemo, useCallback, ReactNode } from 'react'
import { useQuery } from 'react-query'
import { apiRequest } from '../util/apiRequest'

export const USER_QUERY_KEY = 'user'

interface UserContextValue {
  token?: string
  isAuthenticated: boolean
  logout: () => void
}

const UserContext = createContext<UserContextValue>({
  isAuthenticated: false,
  logout: () => {},
})

interface UserProviderProps {
  children: ReactNode
}

export const UserProvider: FC<UserProviderProps> = ({ children }) => {
  const { data: token } = useQuery<string>({
    queryKey: USER_QUERY_KEY,
    enabled: false,
    initialData: '',
  })

  const logout = useCallback(async () => {
    await apiRequest({
      url: '/logout',
      method: 'POST',
      token,
    })

    window.location.href = '/'
  }, [token])

  const value = useMemo(() => ({
    token,
    isAuthenticated: !!token,
    logout,
  }), [token, logout])

  return (
    <UserContext.Provider value={value}>
      {children}
    </UserContext.Provider>
  )
}

export const useUser = () => useContext(UserContext)
