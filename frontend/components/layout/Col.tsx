import styled from '@emotion/styled'

export const Col = styled.div`
  display: flex;
  flex-direction: column;

  &:not(:first-of-type) {
    margin-left: 8px;
  }
`
