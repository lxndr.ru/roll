import { FC } from 'react'
import styled from '@emotion/styled'
import { AppID } from '~/cm/types'

interface SteamAppLogoProps {
  appid: AppID
}

export const SteamAppLogo: FC<SteamAppLogoProps> = ({ appid }) =>
  <Image
    src={`https://cdn.cloudflare.steamstatic.com/steam/apps/${appid}/capsule_184x69.jpg`}
  />

const Image = styled.img`
  width: 184px;
  height: 69px;
`
