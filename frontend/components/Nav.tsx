import { FC } from 'react'
import { NavLink } from 'react-router-dom'
import { css } from '@emotion/react'
import styled from '@emotion/styled'
import { useUser } from '~/fe/providers/user'

export const Nav: FC = () => {
  const { logout } = useUser()

  return (
    <nav>
      <RouterLink to="/">Home</RouterLink>
      <RouterLink to="/games">Games</RouterLink>
      <ActionButton onClick={logout}>Logout</ActionButton>
    </nav>
  )
}

const linkStyle = css`
  color: midnightblue;
  background: none;
  text-decoration: none;
  font-weight: bold;
  border: none;
  display: inline-block;
  padding: 10px;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`

const RouterLink = styled(NavLink)`
  ${linkStyle}
`

const ActionButton = styled.button`
  ${linkStyle}
`
