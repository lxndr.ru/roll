import { FC, DetailedHTMLProps, InputHTMLAttributes } from 'react'
import styled from '@emotion/styled'

interface CheckboxProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  label: string
}

export const Checkbox: FC<CheckboxProps> = ({
  className,
  label,
  ...props
}) =>
  <Label className={className}>
    <Input type="checkbox" {...props} />
    <Text>{label}</Text>
  </Label>

const Label = styled.label`
  line-height: 18px;
`

const Input = styled.input`
  margin: 0;
  vertical-align: middle;
`

const Text = styled.span`
  margin-left: 8px;
  vertical-align: middle;
`
