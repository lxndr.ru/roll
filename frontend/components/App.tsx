import { FC } from 'react'
import { Route, Routes } from 'react-router-dom'
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query'
import styled from '@emotion/styled'
import { Global, css } from '@emotion/react'
import { FrontPage } from '~/fe/pages/front'
import { GameListPage } from '~/fe/pages/game-list'
import { LoginPage } from '~/fe/pages/login'
import { Nav } from '~/fe/components/Nav'
import { UserProvider, useUser } from '~/fe/providers/user'
import { AdminAppsPage } from '~/fe/pages/admin/apps'

const AppBody: FC = () => {
  const { isAuthenticated } = useUser()

  if (!isAuthenticated) {
    return <LoginPage />
  }

  return (
    <Container>
      <Nav />

      <Routes>
        <Route path="/" element={<FrontPage />} />
        <Route path="/games" element={<GameListPage />} />
        <Route path="/admin/apps" element={<AdminAppsPage />} />
      </Routes>
    </Container>
  )
}

interface AppProps {
  queryClient: QueryClient
  dehydratedState?: unknown
}

export const App: FC<AppProps> = ({ queryClient, dehydratedState }) =>
  <QueryClientProvider client={queryClient}>
    <Hydrate state={dehydratedState}>
      <UserProvider>
        <Global styles={globalStyle} />
        <AppBody />
      </UserProvider>
    </Hydrate>
  </QueryClientProvider>

const globalStyle = css`
  html, body {
    font-family: 'Exo 2', sans-serif;
    font-size: 14px;
    margin: 0;
    padding: 0;
  }
`

const Container = styled.div`
  text-align: center;
`
