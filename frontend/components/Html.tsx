import { FC, ReactNode } from 'react'

interface HtmlProps {
  title?: string
  jsBundle?: string
  dehydratedState: unknown
  children: ReactNode
}

const formatIntialState = (state: unknown) =>
  `window.__REACT_QUERY_STATE__ = ${JSON.stringify(state)}`

const gaScript = `
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-BHYVBTE94K');
`

export const Html: FC<HtmlProps> = ({
  title = 'Steam Game Roulette',
  jsBundle = '/bundle.js',
  dehydratedState,
  children,
}) => (
  <html lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>{title}</title>
      <link href="https://fonts.googleapis.com/css?family=Exo+2:400,700&display=swap" rel="stylesheet" />

      <script async src="https://www.googletagmanager.com/gtag/js?id=G-BHYVBTE94K" />
      <script dangerouslySetInnerHTML={{ __html: gaScript }} />
    </head>

    <body>
      <div id="app">{children}</div>

      <script
        type="text/javascript"
        dangerouslySetInnerHTML={{ __html: formatIntialState(dehydratedState) }}
      />

      <script
        type="text/javascript"
        src={jsBundle}
      />
    </body>
  </html>
)
