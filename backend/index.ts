import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import Router from '@koa/router'
import axios from 'axios'
import config from '~/be/config'
import * as db from './db'
import * as coreCtl from './controllers/core'
import * as userCtl from './controllers/user'
import * as restCtl from './controllers/rest'
import * as dataService from './services/data'
import * as steamDataService from './services/steam-data'
import { KoaState } from './types'
import { logger } from './logger'

axios.defaults.baseURL = `http://localhost:${config.http.port}/`

const start = async (): Promise<void> => {
  await db.init()
  void dataService.enqueueUpdater()

  const router = new Router<KoaState>()
    .use(coreCtl.user)
    .get('/auth/login', coreCtl.login)
    .get('/auth/verify', coreCtl.verify)
    .post('/api/logout', coreCtl.logout)
    .get('/api/categories', coreCtl.auth, coreCtl.categories)
    .get('/api/genres', coreCtl.auth, coreCtl.genres)
    .post<unknown, unknown>('/api/games/:appid/mark', coreCtl.auth, userCtl.markGame)
    .post<unknown, unknown>('/api/games/:appid/unmark', coreCtl.auth, userCtl.unmarkGame)
    .get('/api/games', coreCtl.auth, userCtl.games)
    .get<unknown, unknown>('/api/random', coreCtl.auth, userCtl.random)
    .get<unknown, unknown>('/api/rest/:type\\::key', coreCtl.auth, coreCtl.authAdmin, restCtl.get)
    .get<unknown, unknown>('/api/rest/:type', coreCtl.auth, coreCtl.authAdmin, restCtl.list)
    .put<unknown, unknown>('/api/rest/app.info\\::appid', coreCtl.auth, coreCtl.authAdmin, restCtl.updateAppInfo)
    .get('/bundle.(js|css)', coreCtl.bundle)
    .get('/(.*)', coreCtl.page)

  const app = new Koa<KoaState>()
    .use(bodyParser())
    .use(coreCtl.error)
    .use(router.routes())
    .use(router.allowedMethods())
    .listen(config.http.port)

  process.on('SIGTERM', () => {
    app.close()
    dataService.shutdown()
    steamDataService.shutdown()
  })
}

start().catch(err => {
  logger.error(err)
  process.exitCode = 1
})
