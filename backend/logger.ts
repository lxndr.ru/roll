import { createLogger, format, transports } from 'winston'
import config from '~/be/config'

export const logger = createLogger({
  format: format.combine(
    format.colorize(),
    format.timestamp(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`),
  ),
  level: config.logger.level,
  transports: [
    new transports.Console(),
  ],
})
