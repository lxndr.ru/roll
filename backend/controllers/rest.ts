import { NotFound } from 'http-errors'
import { OK } from 'http-status'
import * as yup from 'yup'
import { KoaMiddleware } from '~/be/types'
import * as db from '~/be/db/client'
import { APP_INFO } from '~/be/db/types'

type GetMiddleware = KoaMiddleware<{ type: string }>

export const list: GetMiddleware = async ctx => {
  const { type } = ctx.params

  const { docs } = await db.findEntities({
    selector: {
      type,
    },
    limit: 10000,
  })

  ctx.body = docs
}

export const get: GetMiddleware = async ctx => {
  const { type, key } = ctx.params
  const doc = await db.getEntity(type, key)

  if (!doc) {
    throw new NotFound()
  }

  ctx.body = doc
}

const updateAppInfoParamsSchema = yup.object({
  appid: yup.number().positive().min(0).required(),
})

const updateAppInfoBodySchema = yup.object({
  comment: yup.string().defined(),
  noFinishAchievement: yup.boolean().required(),
  finishAchievements: yup.array(yup.string().required()).required(),
  noGame: yup.boolean().required(),
}).strict()

type UpdateAppInfoMiddleware = KoaMiddleware<{ appid: string }>

export const updateAppInfo: UpdateAppInfoMiddleware = async ctx => {
  const { appid } = await updateAppInfoParamsSchema.validate(ctx.params)
  const appInfo = await updateAppInfoBodySchema.validate(ctx.request.body)

  const id = db.formatID(APP_INFO, appid)
  await db.insertEntity(id, appInfo, { appid })
  ctx.status = OK
}
