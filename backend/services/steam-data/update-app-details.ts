import { AppID } from '~/cm/types'
import * as steam from '~/be/steam'
import * as db from '~/be/db/client'
import { Entity, Meta, NeedsUpdateMeta, RequestMeta } from '~/be/db/types'
import { logger } from '~/be/logger'

export const STEAM_APP_DETAILS = 'steam.app.details'

interface AppDetailsMeta extends Meta, RequestMeta, NeedsUpdateMeta {
  appid: AppID
}

export type AppDetailsEntity = Entity<typeof STEAM_APP_DETAILS, steam.AppDetails | null, AppDetailsMeta>

const updateAppDetails = async (appid: AppID): Promise<void> => {
  const id = db.formatID(STEAM_APP_DETAILS, appid)

  const meta: AppDetailsMeta = {
    needsUpdate: false,
    appid,
    request: {
      ok: true,
      error: null,
    },
  }

  try {
    const details = await steam.getAppDetails({ appids: appid })
    await db.insertEntity(id, details, meta)
  } catch (err: unknown) {
    if (err instanceof Error) {
      logger.warn(`steam.getAppDetails({ appid: ${appid} }) errored: ${err.message}`)

      await db.errorEntity(id, null, {
        ...meta,
        request: {
          ok: false,
          error: err.message,
        },
      })

      return
    }

    throw err
  }
}

export default updateAppDetails
