import { isEqual, keyBy } from 'lodash'
import { IdentifiedDocument } from 'nano'
import { AppID, SteamID } from '~/cm/types'
import * as steam from '~/be/steam'
import * as db from '~/be/db/client'
import { Entity } from '~/be/db/types'
import { AppDetailsEntity, STEAM_APP_DETAILS } from './update-app-details'
import { AppSchemaEntity, STEAM_APP_SCHEMA } from './update-app-schema'
import { STEAM_USER_STATS, UserStatsEntity } from './update-user-stats'

export const STEAM_USER_GAMES = 'steam.user.games'

export type UserGamesEntity = Entity<typeof STEAM_USER_GAMES, steam.OwnedGame[], {
  steamid: SteamID
}>

const getDifference = (newGames: steam.OwnedGame[], oldGames: steam.OwnedGame[]) => {
  const oldGamesMap = keyBy<steam.OwnedGame | undefined>(oldGames, 'appid')
  const newGamesMap = keyBy<steam.OwnedGame | undefined>(newGames, 'appid')
  const newIDs: AppID[] = []
  const updatedIDs: AppID[] = []
  const removedIDs: AppID[] = []

  newGames.forEach(newGame => {
    const oldGame = oldGamesMap[newGame.appid]

    if (oldGame) {
      if (!isEqual(newGame, oldGame)) {
        updatedIDs.push(newGame.appid)
      }
    } else {
      newIDs.push(newGame.appid)
    }
  })

  oldGames.forEach(oldGame => {
    const newGame = newGamesMap[oldGame.appid]

    if (!newGame) {
      removedIDs.push(oldGame.appid)
    }
  })

  return { newIDs, updatedIDs, removedIDs }
}

const updateUserGames = async (steamid: SteamID): Promise<void> => {
  const [newOwnedGames, storedOwnedGames] = await Promise.all([
    steam.getOwnedGames({
      steamid,
      include_appinfo: true,
      include_played_free_games: true,
    }),
    db.getEntity<UserGamesEntity>(STEAM_USER_GAMES, steamid),
  ])

  const id = db.formatID(STEAM_USER_GAMES, steamid)
  const ownedGames = storedOwnedGames?.data || []

  const { newIDs, updatedIDs, removedIDs } = getDifference(newOwnedGames, ownedGames)

  const appids = [...newIDs, ...updatedIDs, ...removedIDs]
    .map(appid => `${STEAM_USER_STATS}:${steamid}-${appid}`)

  if (!appids.length) {
    return
  }

  const statsEntities = await db.getEntities<UserStatsEntity>(appids)

  for (const entity of statsEntities) {
    if (updatedIDs.includes(entity.meta.appid)) {
      entity.meta.needsUpdate = true
    }

    if (removedIDs.includes(entity.meta.appid)) {
      entity._deleted = true
    }
  }

  const entities = statsEntities as
    ((UserGamesEntity | UserStatsEntity | AppDetailsEntity | AppSchemaEntity) & IdentifiedDocument)[]

  for (const appid of newIDs) {
    entities.push({
      _id: `${STEAM_USER_STATS}:${steamid}-${appid}`,
      type: STEAM_USER_STATS,
      updatedAt: 0,
      data: {
        achievements: [],
        stats: [],
      },
      meta: {
        steamid,
        appid,
        needsUpdate: true,
        request: {
          ok: true,
          error: null,
        },
      },
    })

    entities.push({
      _id: `${STEAM_APP_DETAILS}:${appid}`,
      type: STEAM_APP_DETAILS,
      updatedAt: 0,
      data: null,
      meta: {
        appid,
        needsUpdate: true,
        request: {
          ok: true,
          error: null,
        },
      },
    })

    entities.push({
      _id: `${STEAM_APP_SCHEMA}:${appid}`,
      type: STEAM_APP_SCHEMA,
      updatedAt: 0,
      data: {},
      meta: {
        appid,
        needsUpdate: true,
        request: {
          ok: true,
          error: null,
        },
      },
    })
  }

  await db.insertEntities(entities)
  await db.insertEntity(id, newOwnedGames, { steamid })
}

export default updateUserGames
