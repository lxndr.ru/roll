import { AxiosError } from 'axios'
import { AppID } from '~/cm/types'
import * as steam from '~/be/steam'
import * as db from '~/be/db/client'
import { Entity, Meta, NeedsUpdateMeta, RequestMeta } from '~/be/db/types'
import { logger } from '~/be/logger'

export const STEAM_APP_SCHEMA = 'steam.app.schema'

interface AppSchemaMeta extends Meta, RequestMeta, NeedsUpdateMeta {
  appid: AppID
}

export type AppSchemaEntity = Entity<typeof STEAM_APP_SCHEMA, steam.Game | null, AppSchemaMeta>

const updateAppSchema = async (appid: AppID): Promise<void> => {
  const key = db.formatID(STEAM_APP_SCHEMA, appid)

  const meta: AppSchemaMeta = {
    needsUpdate: false,
    appid,
    request: {
      ok: true,
      error: null,
    },
  }

  try {
    const schema = await steam.getSchemaForGame({ appid })
    await db.insertEntity(key, schema, meta)
  } catch (err: unknown) {
    if (err instanceof Error) {
      logger.warn(`steam.getSchemaForGame({ appid: ${appid} }) errored: ${err.message}`)

      if ('isAxiosError' in err) {
        const reqError = err as AxiosError
        const schema: steam.Game = {}

        await db.errorEntity(key, schema, {
          ...meta,
          request: {
            ok: false,
            error: reqError.message,
          },
        })

        return
      }
    }

    throw err
  }
}

export default updateAppSchema
