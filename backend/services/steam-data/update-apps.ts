import ms from 'ms'
import { IdentifiedDocument, MaybeRevisionedDocument } from 'nano'
import { map, set, lensPath } from 'ramda'
import * as db from '~/be/db/client'
import { AppDetailsEntity, STEAM_APP_DETAILS } from './update-app-details'
import { AppSchemaEntity, STEAM_APP_SCHEMA } from './update-app-schema'

type AppEntity = AppDetailsEntity | AppSchemaEntity
type AppEntityDocument = AppEntity & IdentifiedDocument & MaybeRevisionedDocument

const markForUpdate = map(
  set(
    lensPath<AppEntityDocument>(['meta', 'needsUpdate']),
    true,
  ),
)

export const updateApps = async (): Promise<void> => {
  const { docs: details } = await db.findEntities<AppEntity>({
    selector: {
      type: {
        $in: [STEAM_APP_DETAILS, STEAM_APP_SCHEMA],
      },
      updatedAt: {
        $lt: Date.now() - ms('7d'),
      },
    },
    limit: 50,
  })

  const newDetails = markForUpdate(details)
  await db.upsertEntities(newDetails)
}
