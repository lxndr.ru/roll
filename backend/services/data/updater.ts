import _ from 'lodash'
import { Entity } from '~/be/db/types'
import * as db from '~/be/db/client'
import { STEAM_APP_DETAILS } from '~/be/services/steam-data/update-app-details'
import { STEAM_APP_SCHEMA } from '~/be/services/steam-data/update-app-schema'
import { STEAM_USER_GAMES } from '~/be/services/steam-data/update-user-games'
import { STEAM_USER_STATS } from '~/be/services/steam-data/update-user-stats'

import {
  udpateAppDetailsTask,
  updateAppSchemaTask,
  updateUserGamesTask,
  updateUserStatsTask,
} from '../steam-data'

const updateMap: Record<string, {
  func: (...args: unknown[]) => Promise<void>
  argPaths: string[]
}> = {
  [STEAM_APP_DETAILS]: {
    func: udpateAppDetailsTask,
    argPaths: ['appid'],
  },
  [STEAM_APP_SCHEMA]: {
    func: updateAppSchemaTask,
    argPaths: ['appid'],
  },
  [STEAM_USER_GAMES]: {
    func: updateUserGamesTask,
    argPaths: ['steamid'],
  },
  [STEAM_USER_STATS]: {
    func: updateUserStatsTask,
    argPaths: ['steamid', 'appid'],
  },
}

const updater = async (): Promise<boolean> => {
  const result = await db.findEntities<Entity>({
    selector: {
      meta: {
        needsUpdate: true,
      },
    },
    limit: 1,
    use_index: ['apps-index', 'needs-update'],
  })

  if (!result.docs.length) {
    return false
  }

  const { type, meta } = result.docs[0]
  const { func, argPaths } = updateMap[type]
  const args = _.at(meta as Record<string, unknown>, argPaths)
  await func(...args)
  return true
}

export default updater
