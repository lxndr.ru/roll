import _ from 'lodash'
import { App, AppID, Completion } from '~/cm/types'

export class Builder {
  updatedAt = 0

  private readonly _apps = new Map<AppID, App>()

  addAppData(data: App) {
    const app = this._appById(data.appid)
    _.merge(app, data)
  }

  getAppIds(): AppID[] {
    return Array.from(this._apps.keys())
  }

  getApps(): App[] {
    for (const app of this._apps.values()) {
      const achievements = Object.values(app.achievements)

      if (achievements.length) {
        const finished = achievements.some(({ achieved, finish }) => achieved && finish)
        const completed = achievements.every(({ achieved }) => achieved)

        app.hasAchievements = true

        if (finished) {
          app.completion = Completion.finished
        }

        if (completed) {
          app.completion = Completion.completed
        }
      }
    }

    return Array.from(this._apps.values())
  }

  private _appById(appid: AppID): App {
    let app = this._apps.get(appid)

    if (!app) {
      app = {
        appid,
        name: '',
        isFree: false,
        completion: Completion.neverPlayed,
        platforms: [],
        hasAchievements: false,
        achievements: {},
        categories: [],
        genres: [],
      }

      this._apps.set(appid, app)
    }

    return app
  }
}
