import _ from 'lodash'
import { MangoQuery } from 'nano'
import { Genre } from '~/cm/types'
import * as db from '~/be/db/client'
import * as steam from '~/be/steam'
import { AppDetailsEntity, STEAM_APP_DETAILS } from '~/be/services/steam-data/update-app-details'

const blocklist: string[] = [
  'Animation & Modeling',
  'Audio Production',
  'Design & Illustration',
  'Game Development',
  'Movie',
  'Photo Editing',
  'Software Training',
  'Utilities',
  'Video Production',
  'Web Publishing',
]

let updatedAt = 0
let genres: Genre[] = []

export const getGenres = async (): Promise<Genre[]> => {
  const newBatch: steam.Genre[][] = []

  const query: MangoQuery = {
    selector: {
      'type': STEAM_APP_DETAILS,
      'updatedAt': {
        $gt: updatedAt,
      },
      'data.genres': {
        $type: 'array',
      },
    },
    fields: [
      'data.genres',
    ],
    use_index: ['apps-index', 'genres'],
  }

  for await (const details of db.find<AppDetailsEntity>(query)) {
    if (details.data?.genres) {
      newBatch.push(details.data.genres)
    }
  }

  if (!newBatch.length) {
    return genres
  }

  genres = _(newBatch)
    .flatten()
    .uniqBy('id')
    .map<Genre>(({ id, description }) => ({ id: Number(id), name: description }))
    .concat(genres)
    .uniqBy('id')
    .filter(({ name }) => !blocklist.includes(name))
    .sortBy('name')
    .value()

  updatedAt = Date.now()
  return genres
}

export default getGenres
