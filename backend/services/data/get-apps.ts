import { App, SteamID } from '~/cm/types'
import * as db from '~/be/db/client'
import { Builder } from './get-apps.builder'

const apps = new Map<SteamID, Builder>()

export const getApps = async (steamid: SteamID): Promise<App[]> => {
  let builder = apps.get(steamid)

  if (!builder) {
    builder = new Builder()
    apps.set(steamid, builder)
  }

  const userGames = await db.fetchView<App>('apps', 'user-games', {
    startkey: [steamid, builder.updatedAt],
    endkey: [steamid, {}],
    sorted: false,
  })

  for (const userGame of userGames) {
    builder.addAppData(userGame.value)
  }

  const appIds = builder.getAppIds()

  const appInfos = await db.fetchView<App>('apps', 'apps', {
    keys: appIds,
    sorted: false,
  })

  for (const appInfo of appInfos) {
    builder.addAppData(appInfo.value)
  }

  builder.updatedAt = Date.now()
  return builder.getApps()
}

export default getApps
