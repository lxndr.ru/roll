import createNano, {
  CreateIndexRequest,
  Document,
  DocumentGetResponse,
  DocumentResponseRow,
  DocumentViewParams,
  IdentifiedDocument,
  MangoQuery,
  MangoResponse,
  MaybeRevisionedDocument,
  RequestError,
} from 'nano'

import config from '~/be/config'
import { logger } from '~/be/logger'
import { Entity, Meta } from './types'

const nano = createNano({
  url: config.db.host,
  requestDefaults: {
    auth: {
      username: config.db.user,
      password: config.db.pass,
    },
  },
})

const db = nano.use<Entity>(config.db.name)

export const isCouchError = (err: unknown): err is RequestError => {
  if (err instanceof Error && 'scope' in err) {
    const reqErr = err as RequestError

    if (reqErr.scope === 'couch') {
      return true
    }
  }

  return false
}

export const createDatabase = async () => {
  const dblist = await nano.db.list()

  if (!dblist.includes(config.db.name)) {
    await nano.db.create(config.db.name)
  }
}

export const formatID = (type: string, ...args: (string | number | null)[]) => {
  const keys = args.filter(key => key != null)

  if (keys.length) {
    const id = keys.join('-')
    return `${type}:${id}`
  }

  return type
}

export const insertEntity = async <T, M extends Meta>(id: string, data: T, meta: M) => {
  await db.updateWithHandler('entity', 'insert', id, { data, meta })
}

export const errorEntity = async <T, M extends Meta>(id: string, data: T, meta: M) => {
  await db.updateWithHandler('entity', 'error', id, { data, meta })
}

export const deleteEntity = async (id: string) => {
  await db.updateWithHandler('entity', 'delete', id)
}

export const insertEntities = async <T extends Entity>(entities: T[]) => {
  await db.bulk({ docs: entities })
}

export const upsertEntities = async <T extends Entity>(
  entities: (T & IdentifiedDocument & MaybeRevisionedDocument)[],
) => {
  while (entities.length) {
    logger.info(`bulk upsert ${entities.length} entities`)

    const keys = entities.map(entity => entity._id)
    const revResults = await db.fetchRevs({ keys })
    const revs = revResults.rows
      .filter((row): row is DocumentResponseRow<T> => 'doc' in row)
      .map(row => row.value.rev)

      entities = entities.map((doc, index) => ({
      ...doc,
      _rev: revs[index],
    }))

    const results = await db.bulk({ docs: entities })

    entities = entities.filter(entity =>
      results.find(({ id, error }) =>
        entity._id === id && error === 'conflict'))
  }
}

export const getEntities = async <T extends Entity>(keys: string[]): Promise<(T & Document)[]> => {
  const results = await db.fetch({ keys })

  return results.rows
    .filter((row): row is DocumentResponseRow<T> =>
      'doc' in row && !!row.doc)
    .map(row => row.doc)
    .filter((doc): doc is (T & Document) => !!doc && !doc._deleted)
}

export const getEntity = async <T extends Entity>(
  type: string,
  key: string | null = null,
): Promise<(DocumentGetResponse & T) | null> => {
  try {
    const _id = formatID(type, key)
    const doc = await db.get(_id)
    return doc as (DocumentGetResponse & T)
  } catch (err: unknown) {
    if (isCouchError(err) && err.error === 'not_found') {
      return null
    }

    throw err
  }
}

export const findEntities = async <T extends Entity>(query: MangoQuery): Promise<MangoResponse<T>> =>
  await db.find(query) as MangoResponse<T>

export const createIndex = async (index: CreateIndexRequest): Promise<void> => {
  await db.createIndex(index)
}

// eslint-disable-next-line func-style
export async function* find<T extends Entity>(query: MangoQuery): AsyncGenerator<T, void> {
  const q = { ...query, limit: 1000 }
  let res: MangoResponse<T>

  do {
    res = await findEntities<T>(q)
    const { docs, bookmark } = res
    q.bookmark = bookmark

    for (let i = 0, len = docs.length; i < len; i++) {
      yield docs[i]
    }
  } while (res.docs.length >= q.limit)
}

export const fetchView = async <T>(designname: string, viewname: string, params: DocumentViewParams) => {
  const body = await db.view<T>(designname, viewname, params)
  return body.rows
}
