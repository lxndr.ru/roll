import { AppID, SteamID, UserApp } from '~/cm/types'

export const USER_APP = 'user.app'
export const APP_INFO = 'app.info'

export interface Meta {}

export interface RequestMeta {
  request: {
    ok: boolean
    error: string | null
  }
}

export interface NeedsUpdateMeta {
  needsUpdate: boolean
}

export interface Entity<
  T extends string = string,
  D = unknown,
  M extends Meta = Meta,
> {
  type: T
  updatedAt: number
  data: D
  meta: M
  _deleted?: boolean
}

export interface AppInfo {
  comment: string
  noFinishAchievement: boolean
  finishAchievements: string[]
  noGame: boolean
}

export type AppInfoEntity = Entity<typeof APP_INFO, AppInfo, {
  appid: AppID
}>

export type UserAppEntity = Entity<typeof USER_APP, UserApp, {
  steamid: SteamID
  appid: AppID
}>
