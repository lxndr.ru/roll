import {
  App,
  AppFilter,
  AppID,
  Category,
  Completion,
  CompletionFilter,
  Genre,
  SteamID,
  UserAppStatus,
} from '~/cm/types'

import {
  createDatabase,
  createIndex,
  deleteEntity,
  getEntity,
  insertEntity,
} from './client'

import indexes from '~/be/db-entities/indexes'
import { getAppsTask, getCategoriesTask, getGenresTask } from '~/be/services/data'
import { USER_APP, UserAppEntity } from './types'

export const init = async (): Promise<void> => {
  await createDatabase()

  for (const index of indexes) {
    await createIndex(index)
  }
}

export const getCategories = async (): Promise<Category[]> =>
  await getCategoriesTask()

export const getGenres = async (): Promise<Genre[]> =>
  await getGenresTask()

export const getApps = async (filter: AppFilter): Promise<App[]> => {
  const apps = await getAppsTask(filter.steamid)

  return apps.filter(it => {
    if (filter.skipIgnored && it.userStatus === UserAppStatus.ignored) {
      return false
    }

    if ('includeFree' in filter) {
      if (!filter.includeFree && it.isFree) {
        return false
      }
    }

    if (filter.categories?.length) {
      if (!filter.categories.some(category => it.categories.includes(category))) {
        return false
      }
    }

    if (filter.genres?.length) {
      if (!filter.genres.some(genre => it.genres.includes(genre))) {
        return false
      }
    }

    if (filter.platform) {
      if (!it.platforms.includes(filter.platform)) {
        return false
      }
    }

    if ('achievementsOnly' in filter) {
      if (filter.achievementsOnly && !it.hasAchievements) {
        return false
      }
    }

    if (filter.completion && filter.completion !== CompletionFilter.all) {
      switch (filter.completion) {
        case CompletionFilter.neverPlayed:
          if (it.completion !== Completion.neverPlayed) {
            return false
          }
          break
        case CompletionFilter.played:
          if (it.completion === Completion.neverPlayed) {
            return false
          }
          break
        case CompletionFilter.unfinished:
          if (it.completion !== Completion.played ||
              it.userStatus === UserAppStatus.finished ||
              it.userStatus === UserAppStatus.completed) {
            return false
          }
          break
        case CompletionFilter.uncompleted:
          if (it.completion !== Completion.finished ||
              it.userStatus === UserAppStatus.completed) {
            return false
          }
          break
        default:
      }
    }

    return true
  })
}

export const markGame = async (steamid: SteamID, appid: AppID, status: UserAppStatus): Promise<void> => {
  const key = `${USER_APP}:${steamid}-${appid}`
  const meta = { steamid, appid }
  const existingEntity = await getEntity<UserAppEntity>(USER_APP, key)
  const data = existingEntity?.data || { status }
  await insertEntity(key, { ...data, status }, meta)
}

export const unmarkGame = async (steamid: SteamID, appid: AppID): Promise<void> => {
  const key = `${USER_APP}:${steamid}-${appid}`
  await deleteEntity(key)
}
