function (doc, req) {
  if (!doc) {
    return [null, 'wrong_docid']
  }

  doc._id = req.id
  doc._deleted = true
  return [doc, 'ok']
}
