function ({ type, data, updatedAt, meta }) {
  if (!data) {
    return;
  }

  const _processUserGames = () => {
    if (!Array.isArray(data)) {
      return;
    }

    data.forEach(({ appid, name, playtime_forever }) => {
      emit([meta.steamid, updatedAt], {
        appid,
        name,
        playtime: playtime_forever,
        completion: playtime_forever ? 'played' : 'neverPlayed',
      });
    });
  };
  
  const _processUserStats = () => {
    if (!(Array.isArray(data.achievements) && data.achievements.length)) {
      return;
    }

    const { steamid, appid } = meta;
    const achievements = {};

    data.achievements.forEach(({ name, achieved }) => {
      achievements[name] = {
        achieved: Boolean(achieved),
      };
    });

    emit([steamid, updatedAt], {
      appid,
      achievements,
    });
  };

  const _processAppInfo = () => {
    if (!data.status) {
      return;
    }
    
    const { steamid, appid } = meta;

    emit([steamid, updatedAt], {
      appid,
      userStatus: data.status,
    });
  };

  switch (type) {
  case 'steam.user.games':
    _processUserGames();
    break;
  case 'steam.user.stats':
    _processUserStats();
    break;
  case 'user.app':
    _processAppInfo();
    break;
  }
}
