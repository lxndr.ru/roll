function ({ type, data, meta }) {
  if (!data) {
    return;
  }

  const extractPlatforms = (platforms) =>
    Object.keys(platforms).filter(platform => platforms[platform]);

  const _processAppDetails = () => {
    const { appid } = meta;
    const { is_free, platforms, categories, genres } = data;

    emit(appid, {
      appid,
      isFree: is_free,
      platforms: extractPlatforms(platforms),
      categories: Array.isArray(categories)
        ? categories.map(({ id }) => Number(id))
        : [],
      genres: Array.isArray(genres)
        ? genres.map(({ id }) => Number(id))
        : [],
    });
  };

  const _processAppSchema = () => {
    if (!(data.availableGameStats && data.availableGameStats.achievements)) {
      return;
    }

    const { achievements: availableAchievements } = data.availableGameStats;

    if (!(Array.isArray(availableAchievements) && availableAchievements.length)) {
      return;
    }

    const { appid } = meta;
    const achievements = {};

    availableAchievements.forEach(({ name, displayName }) => {
      achievements[name] = {
        name: displayName,
      };
    });

    emit(appid, {
      appid,
      achievements,
    });
  };

  const _processAppInfo = () => {
    const { finishAchievements } = data;

    if (!(Array.isArray(finishAchievements) && finishAchievements.length)) {
      return;
    }

    const { appid } = meta;
    const achievements = {};

    finishAchievements.forEach(id => {
      achievements[id] = {
        finish: true,
      };
    });

    emit(appid, {
      appid,
      achievements,
    });
  };

  switch (type) {
  case 'steam.app.details':
    _processAppDetails();
    break;
  case 'steam.app.schema':
    _processAppSchema();
    break;
  case 'app.info':
    _processAppInfo();
    break;
  }
}
