import { nodeCrypto, pick } from 'random-js'

export const randomArrayItem = <T>(array: T[]): T | null => {
  if (!array.length) {
    return null
  }

  return pick<T>(nodeCrypto, array)
}
