import { fetch } from './request'
import { AppID, SteamID } from '~/cm/types'

/* eslint-disable @typescript-eslint/naming-convention */

export interface GetOwnedGamesParams {
  steamid: SteamID
  include_appinfo?: boolean
  include_played_free_games?: boolean
  appids_filter?: number[]
}

export interface OwnedGame {
  appid: AppID
  name: string
  playtime_2weeks?: number
  playtime_forever: number
  playtime_windows_forever: number
  playtime_mac_forever: number
  playtime_linux_forever: number
  img_icon_url: string
  img_logo_url: string
  has_community_visible_stats: boolean
}

export interface GetOwnedGamesResult {
  response: {
    game_count: number
    games: OwnedGame[]
  }
}

export const getOwnedGames = async (params: GetOwnedGamesParams): Promise<OwnedGame[]> => {
  const { response } = await fetch<GetOwnedGamesResult>('IPlayerService/GetOwnedGames/v0001', params)
  return response.games || []
}
