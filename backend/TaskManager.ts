import { performance } from 'perf_hooks'
import { nanoid } from 'nanoid'
import defer, { DeferredPromise } from 'p-defer'
import { pull } from 'lodash'
import ms from 'ms'
import { logger } from './logger'

export interface TaskManagerOptions {
  concurrency?: number
}

export type TaskFunction<T> = () => Promise<T>

export enum TaskPriority {
  low = -1,
  normal = 0,
  high = 1,
}

interface Task<T> {
  key: string
  func: TaskFunction<T>
  deferred: DeferredPromise<T>
  priority: TaskPriority
  running: boolean
}

export type TaskOptions = Partial<Pick<Task<never>, 'key' | 'priority'>>

export class TaskManager {
  private readonly tasks: Task<unknown>[] = []

  private readonly concurrency = Infinity

  private closed = false

  constructor({
    concurrency = Infinity,
  }: TaskManagerOptions = {}) {
    this.concurrency = concurrency
  }

  async enqueueTask<T = never>(func: TaskFunction<T>, options: TaskOptions = {}): Promise<T> {
    const key = options.key || nanoid(10)
    const priority = options.priority || TaskPriority.normal
    const existingTask = this.tasks.find<Task<T>>((task): task is Task<T> => task.key === key)

    if (existingTask) {
      return existingTask.deferred.promise
    }

    const task = {
      func,
      key,
      priority,
      deferred: defer<T>(),
      running: false,
    }

    this.tasks.push(task)
    this.wakeup()
    return task.deferred.promise
  }

  wakeup(): void {
    if (this.closed) {
      return
    }

    let curentlyRunning = this.tasks.reduce(
      (count, task) => (task.running ? count + 1 : count),
      0,
    )

    while (curentlyRunning < this.concurrency) {
      const task = this.tasks.find(task => !task.running)

      if (!task) {
        break
      }

      void this._runTask(task)
      task.running = true
      curentlyRunning++
    }
  }

  shutdown(): void {
    this.closed = true
  }

  private async _runTask(task: Task<unknown>) {
    logger.info(`task '${task.key}' started`)
    const startTime = performance.now()

    try {
      const res = await task.func()
      task.deferred.resolve(res)
    } catch (err) {
      task.deferred.reject(err)
    } finally {
      const endTime = performance.now()
      logger.info(`task '${task.key}' finished ${ms(endTime - startTime)}`)

      pull(this.tasks, task)
      this.wakeup()
    }
  }
}
